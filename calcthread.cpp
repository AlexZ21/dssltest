#include "calcthread.h"
#include "circleitem.h"

#include <iostream>
#include <math.h>

pthread_mutex_t CalcThread::circlesMutex = PTHREAD_MUTEX_INITIALIZER;

CalcThread::CalcThread() :
    _circles(0)
{
}

CalcThread::~CalcThread()
{
}

void CalcThread::start()
{
    _run = true;
    pthread_create(&_pthread, 0, CalcThread::threadFunc, (void*)this);
}

void CalcThread::stop()
{
    _run = false;
}

void CalcThread::wait()
{
    pthread_join(_pthread, 0);
}

void CalcThread::calc()
{
    while (_run) {
        pthread_mutex_lock(&circlesMutex);

        for (std::list<CircleItem *>::iterator it=_circles->begin(); it != _circles->end(); ++it) {
            double deltaX = 0;
            double deltaY = 0;

            for (std::list<CircleItem *>::iterator subIt=_circles->begin(); subIt != _circles->end(); ++subIt) {
                if ((*it) != (*subIt)) {
                    std::pair<double, double> deltaPos = calc((*it), (*subIt));
                    deltaX += deltaPos.first;
                    deltaY += deltaPos.second;
                }
            }
            if(deltaX != 0 && deltaY != 0)
                (*it)->setNewPos((*it)->pos().x() + deltaX, (*it)->pos().y() + deltaY);
        }

        pthread_mutex_unlock(&circlesMutex);
    }
}

void *CalcThread::threadFunc(void *d)
{
    static_cast<CalcThread*>(d)->calc();
    return 0;
}

std::pair<double, double> CalcThread::calc(CircleItem *c1, CircleItem *c2)
{
    double newX = 0;
    double newY = 0;
    double lengthX =  c2->x() - c1->x();
    double lengthY =  c2->y() - c1->y();

    if (lengthX !=0 || lengthY!=0){
         double radAngle = atan2(lengthX,lengthY);
         double r = sqrt(pow(lengthX,2)+pow(lengthY,2));
         double F = (1/r - 1/pow(r,2));
         newX = F*cos(radAngle)*1000;
         newY = F*sin(radAngle)*1000;
    }

    return std::pair<double,double>(newX, newY);
}

void CalcThread::setCircles(std::list<CircleItem *> *circles)
{
    _circles = circles;
}
