#include "mainscene.h"
#include "circleitem.h"
#include "calcthread.h"

#include <QEvent>
#include <QMouseEvent>
#include <QGraphicsSceneMouseEvent>
#include <QTime>
#include <QDebug>

MainScene::MainScene(QObject *parent) :
    QGraphicsScene(parent),
    _selectedItem(0),
    _dragState(false)
{
    installEventFilter(this);

    connect(&_updatingTimer, &QTimer::timeout, [=](){
        for (std::list<CircleItem *>::iterator it = _circles.begin(); it != _circles.end(); ++it)
            if (!(*it)->newPos().isNull())
                (*it)->moveToNewPos();
    });

    _updatingTimer.start(10);
}

MainScene::~MainScene()
{
}

bool MainScene::eventFilter(QObject *watched, QEvent *event)
{
    Q_UNUSED(watched)

    if (event->type() == QEvent::GraphicsSceneMousePress) {
        QGraphicsSceneMouseEvent *me = static_cast<QGraphicsSceneMouseEvent *>(event);

        if (me->button() == Qt::LeftButton) {
            // Если есть круг под курсором, то выбираем его
            QTransform tr;
            QGraphicsItem *i = itemAt(me->scenePos(), tr);

            if (i) _selectedItem = i;
        }

        return false;
    } else if (event->type() == QEvent::GraphicsSceneMouseRelease) {
        QGraphicsSceneMouseEvent *me = static_cast<QGraphicsSceneMouseEvent *>(event);

        // Если нажата правая кнопка мыши и не выбран круг, то добавляется новый круг
        if (me->button() == Qt::RightButton && !_selectedItem) {
            CircleItem *i = new CircleItem();
            i->setPos(me->scenePos());
            i->setNewPos(me->scenePos());
            addItem(i);
            _circles.push_back(i);
        } else if (me->button() == Qt::LeftButton) {
            // Если выбран круг и он не перетаскивается, то он удаляется
            if (_selectedItem && !_dragState) {
                // Блокируем список шаров
                pthread_mutex_lock(&CalcThread::circlesMutex);
                _circles.remove(static_cast<CircleItem *>(_selectedItem));
                delete _selectedItem;
                _selectedItem = 0;
                pthread_mutex_unlock(&CalcThread::circlesMutex);
            }

            // Если перетаскивается круг, то сбрасывается состояние перемещения круга и выбранный круг
            if (_dragState) {
                _selectedItem = 0;
                _dragState = false;
            }
        }

        return false;
    } else if (event->type() == QEvent::GraphicsSceneMouseMove) {
        QGraphicsSceneMouseEvent *me = static_cast<QGraphicsSceneMouseEvent *>(event);

        // Если выбран круг, то устанавливается состояние перемещения, а у круга обновляется позиция
        if (_selectedItem) {
            if (!_dragState) _dragState = true;
            _selectedItem->setPos(me->scenePos());
            static_cast<CircleItem *>(_selectedItem)->setNewPos(me->scenePos());
            static_cast<CircleItem *>(_selectedItem)->stopSmoothMoving();
        }

        return false;
    }

    return false;
}

void MainScene::generateRandom()
{
    QTime time = QTime::currentTime();
    qsrand(time.msec());
    QRectF r = sceneRect();

    for (int i; i < 10; ++i) {
        int x = qrand() % (int)r.width() + ((int)r.x());
        int y = qrand() % (int)r.height() + ((int)r.y());

        CircleItem *item = new CircleItem();
        item->setPos(x, y);
        item->setNewPos(x, y);
        addItem(item);
        _circles.push_back(item);
    }
}

void MainScene::clearScene()
{
    // Блокируем список шаров
    pthread_mutex_lock(&CalcThread::circlesMutex);
    _circles.clear();
    clear();
    pthread_mutex_unlock(&CalcThread::circlesMutex);
}

std::list<CircleItem *> *MainScene::circles()
{
    return &_circles;
}
