#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "calcthread.h"

class MainScene;
class QGraphicsView;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    QGraphicsView *_view;
    MainScene *_mainScene;

    CalcThread _calcThread;
};

#endif // MAINWINDOW_H
