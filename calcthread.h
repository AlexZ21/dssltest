﻿#ifndef CALCTHREAD_H
#define CALCTHREAD_H

#include <pthread.h>
#include <list>
#include <map>

class CircleItem;

class CalcThread
{
public:
    CalcThread();
    ~CalcThread();

    void start();
    void stop();
    void wait();
    void calc();

    void setCircles(std::list<CircleItem *> *circles);

private:
    static void *threadFunc(void *d);
    std::pair<double, double> calc(CircleItem *c1, CircleItem *c2);

public:
    static pthread_mutex_t circlesMutex;

private:
    pthread_t _pthread;
    bool _run;
    std::list<CircleItem *> *_circles;

};

#endif // CALCTHREAD_H
