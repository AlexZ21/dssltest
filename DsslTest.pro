#-------------------------------------------------
#
# Project created by QtCreator 2016-08-02T19:08:15
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DsslTest
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mainscene.cpp \
    circleitem.cpp \
    calcthread.cpp

HEADERS  += mainwindow.h \
    mainscene.h \
    circleitem.h \
    calcthread.h
