#include "mainwindow.h"
#include "mainscene.h"
#include "circleitem.h"

#include <QGraphicsView>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setCentralWidget(new QWidget(this));
    QVBoxLayout *mainlayout = new QVBoxLayout();
    centralWidget()->setLayout(mainlayout);

    resize(500, 400);

    _view = new QGraphicsView(centralWidget());
    mainlayout->addWidget(_view, 1);

    _mainScene = new MainScene();
    _mainScene->setSceneRect(QRect(-300, -300, 600, 600));
    _view->setScene(_mainScene);

    QHBoxLayout *controlsLayout = new QHBoxLayout();
    mainlayout->addLayout(controlsLayout);

    QPushButton *randomGenButton = new QPushButton("Сгенерировать круги", centralWidget());
    connect(randomGenButton, &QPushButton::clicked, _mainScene, &MainScene::generateRandom);
    controlsLayout->addStretch(1);
    controlsLayout->addWidget(randomGenButton);

    QPushButton *clearButton = new QPushButton("Очистить поле", centralWidget());
    connect(clearButton, &QPushButton::clicked, _mainScene, &MainScene::clearScene);
    controlsLayout->addWidget(clearButton);
    controlsLayout->addStretch(1);

    _calcThread.setCircles(_mainScene->circles());
    _calcThread.start();

}

MainWindow::~MainWindow()
{
    _calcThread.stop();
    _calcThread.wait();
}
