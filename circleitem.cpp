#include "circleitem.h"

#include <QPropertyAnimation>
#include <QBrush>

CircleItem::CircleItem(QGraphicsItem *parent) :
    QGraphicsEllipseItem(parent),
    _anim(0)
{
    setRect(0, 0, 20, 20);
    setBrush(QBrush(Qt::red));

    _anim = new QPropertyAnimation(this, "pos");
    _anim->setDuration(1000);
}

CircleItem::~CircleItem()
{
    delete _anim;
}

void CircleItem::moveSmoothly(double x, double y)
{
    moveSmoothly(QPointF(x, y));
}

void CircleItem::moveSmoothly(const QPointF &newPos)
{
    stopSmoothMoving();

    _anim->setStartValue(pos());
    _anim->setEndValue(newPos);
    _anim->start();
}

void CircleItem::stopSmoothMoving()
{
    if (_anim->state() == QAbstractAnimation::Running)
        _anim->stop();
}

bool CircleItem::drag() const
{
    return _drag;
}

void CircleItem::setDrag(bool drag)
{
    _drag = drag;
}

void CircleItem::setNewPos(double x, double y)
{
    _newPos = QPointF(x, y);
}

void CircleItem::setNewPos(const QPointF &newPos)
{
    _newPos = newPos;
}

void CircleItem::moveToNewPos()
{
    if (pos() != _newPos)
        moveSmoothly(_newPos);
}

QPointF CircleItem::newPos() const
{
    return _newPos;
}

