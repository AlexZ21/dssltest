#ifndef CIRCLEITEM_H
#define CIRCLEITEM_H

#include <QGraphicsEllipseItem>
#include <QObject>

class QPropertyAnimation;

class CircleItem : public QObject, public QGraphicsEllipseItem
{
    Q_OBJECT

    Q_PROPERTY(QPointF pos READ pos WRITE setPos)

public:
    CircleItem(QGraphicsItem *parent = 0);
    ~CircleItem();

    void moveSmoothly(double x, double y);
    void moveSmoothly(const QPointF &newPos);
    void stopSmoothMoving();

    bool drag() const;
    void setDrag(bool drag);

    QPointF newPos() const;
    void setNewPos(double x, double y);
    void setNewPos(const QPointF &newPos);

    void moveToNewPos();

private:
    bool _drag;
    QPropertyAnimation *_anim;
    QPointF _newPos;
};

#endif // CIRCLEITEM_H
