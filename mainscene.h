#ifndef MAINSCENE_H
#define MAINSCENE_H

#include <QGraphicsScene>
#include <QTimer>
#include <list>

class CircleItem;

class MainScene : public QGraphicsScene
{
public:
    MainScene(QObject *parent = 0);
    ~MainScene();

    std::list<CircleItem *> *circles();

protected:
    bool eventFilter(QObject *watched, QEvent *event);

public slots:
    void generateRandom();
    void clearScene();

private:
    QGraphicsItem *_selectedItem;
    bool _dragState;
    QTimer _updatingTimer;
    std::list<CircleItem *> _circles;

};

#endif // MAINSCENE_H
